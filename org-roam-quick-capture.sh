#!/bin/bash

# variables
UUID=$(uuidgen)
DATE=$(date +%Y%m%d%H%M%S)
TITLE=$*
TEMP_FILENAME=/tmp/$DATE.org
FILENAME_TITLE=${TITLE// /_}                                   # spaces to underscores
FILENAME_TITLE=${FILENAME_TITLE//-/_}                          # hyphens to underscore
FILENAME_TITLE=$(echo "$FILENAME_TITLE" | tr -s '_' '_')       # one or more underscores to a single underscore
FILENAME_TITLE=${FILENAME_TITLE,,}                             # uppercase to lowercase
FILENAME_TITLE=$(echo "$FILENAME_TITLE" | tr -cd '[:alnum:]_') # remove all non-alphanumeric characters except for underscores
FILENAME=~/Documents/org/roam/$DATE-$FILENAME_TITLE.org
read -r -d '' METADATA <<EOM
:PROPERTIES:
:ID:       ${UUID}
:END:
#+title: ${TITLE}
EOM

# check if arguments have been given
[[ -z "$1" ]] && {
	echo "Supply me with the title of the note!"
	echo "Example: org-roam-quick-capture.sh hello world"
	exit 1
}

# creating and editing the file
echo -e "${METADATA}\n" >"$TEMP_FILENAME"

MD5_ORIGINAL=$(md5sum "$TEMP_FILENAME")

nvim "$TEMP_FILENAME"

MD5_NEW=$(md5sum "$TEMP_FILENAME")

if [[ "$MD5_ORIGINAL" != "$MD5_NEW" ]]; then
	mv "$TEMP_FILENAME" "$FILENAME"
fi
